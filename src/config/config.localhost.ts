const config = {
  env: 'localhost',
  swagger: {
    host: 'localhost',
    schemes: ['https']
  },
  jwt: {
    secret: 'place_random_secret',
  },
  // mongodb: {
  //   connection: 'cluster0-pb8it.gcp.mongodb.net/test?retryWrites=true&w=majority',
  //   host: '184.22.248.139',
  //   port: 27017,
  //   username: 'LineBotTestAdmin',
  //   password: 'P@ssw0rd',
  //   database: 'LineBotTest',
  //   qs: '',
  // },
  line: {
    channelAccessToken: 'VwgywBYt2kRd0YViikeZVEHcDIztRKccGYRGXTu2b4+JpjV1P+j/ggDcHhmF/hwjvfVS2Sr0e+IaDCFgj/i9nrGO9sBKt3ZavcVXf7qPlzyDgpSK3JbbDZVhv1Nn6qiJV8WtRO16xQix2g9n1HHJcgdB04t89/1O/w1cDnyilFU=',
    channelSecret: 'a507e3f3ae9713a24c6b7c634003add9',
  },
  firebaseConfig: {
    apiKey: 'AIzaSyBWJPBAV9vPYSav392na6pXOv0jYBqaGgU',
    authDomain: 'chatbot-line-api.firebaseapp.com',
    databaseURL: 'https://chatbot-line-api.firebaseio.com',
    projectId: 'chatbot-line-api',
    storageBucket: 'chatbot-line-api.appspot.com',
    messagingSenderId: '692844517447'
  },
};

export default config;
