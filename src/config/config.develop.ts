const config = {
  env: 'develop',
  swagger: {
    host: 'api.emetworks-dev.tech',
    schemes: ['http'],
  },
  jwt: {
    secret: 'place_random_secret',
  },
  mongodb: {
    connection: '',
    host: '',
    port: 27017,
    username: '',
    password: '',
    database: '',
    qs: '',
  },
  line: {
    channelAccessToken: '',
    channelSecret: '',
  },
};

export default config;
