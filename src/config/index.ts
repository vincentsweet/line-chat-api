import localhost from './config.localhost';
import develop from './config.develop';
import production from './config.production';

const env = process.env.NODE_ENV || 'localhost';
console.log('Environment =----> ', env);

const setupConfig = env => {
  let config = localhost;
  switch (env) {
    case 'production':
      this.config = production;
      break;
    case 'develop':
      this.config = develop;
      break;
    case 'localhost':
      this.config = localhost;
      break;
  }

  return this.config;
};

export const config = setupConfig(env);
export default config;
