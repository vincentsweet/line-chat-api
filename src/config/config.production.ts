const config = {
  env: 'production',
  swagger: {
    host: 'localhost:3001',
    schemes: ['http'],
  },
  jwt: {
    secret: 'place_random_secret',
  },
  mongodb: {
    connection: '',
    host: '',
    port: 27017,
    username: '',
    password: '',
    database: '',
    qs: '',
  },
  line: {
    channelAccessToken:
      '',
    channelSecret: '',
  },
};

export default config;
