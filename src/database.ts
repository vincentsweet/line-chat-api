import * as mongoose from 'mongoose';
import chalk from 'chalk';
import config from 'config';
import { consoleLog } from 'utility/common';
import * as firebase from 'firebase-admin';

export default connectedCallback => {
  const connected = chalk.bold.cyan;
  const error = chalk.bold.yellow;
  const disconnected = chalk.bold.red;

  // mongoose.connect(config.mongodb.connection, { useNewUrlParser: true });
  // if (config.env !== 'production') {
  //   mongoose.set('debug', true);
  // }
  // mongoose.pluralize(null);
  // const db = mongoose.connection;

  // db.on('connected', () => {
  //   consoleLog(
  //     connected(
  //       'Mongoose default connection is open to ',
  //       config.mongodb.connection,
  //     ),
  //   );
  // });

  // db.on('error', err => {
  //   consoleLog(
  //     error('Mongoose default connection has occured ' + err + ' error'),
  //   );
  // });

  // db.on('disconnected', () => {
  //   consoleLog(disconnected('Mongoose default connection is disconnected'));
  // });

  // db.once('open', connectedCallback);
};
