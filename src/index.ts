'use strict';
import * as fastify from 'fastify';
import * as readpkg from 'read-pkg';
import * as humps from 'humps';
import { AddressInfo } from 'net';
import { initModuleAlias } from './moduleAlias';
const config = require('./config/config.localhost');

initModuleAlias();

import { errorHandler } from './exception';
import swagger, { swaggerOptions } from './swagger';
import initDatabase from './database';
import userRouterV1 from './router/v1/user';
import webhookRouterV1 from './router/v1/webhook';
import getUserBylineUserId from 'router/v1/user/getUserBylineUserId';
import address from 'router/v1/user/schema/address';
import { lineHandler } from 'router/v1/webhook/line';
import { request } from 'https';
import * as admin from 'firebase-admin';

const startServer = async(listeningPort: number) => {
  console.log('Server Start' + 'port number: ' + listeningPort);
  const routerV1 = (fastifyInstance: fastify.FastifyInstance, opt, next) => {
    fastifyInstance.register(webhookRouterV1, { prefix: '/webhook' });
    fastifyInstance.register(userRouterV1, { prefix: '/users' });
    next();
  };

  console.log('Start initDatabase');
  try {
    const app = fastify({ logger: false });
    app.register(require('fastify-cors'));
    app.register(require('fastify-blipp'));
    app.register(require('fastify-formbody'));
    app.register(require('fastify-multipart'), {
      addToBody: true,
    });
    app.register(swagger, swaggerOptions);

    app.addHook('preHandler', (request, reply, next) => {
      request.body = humps.camelizeKeys(request.body || {});
      next();
    });

    app.get('/', async (req, reply) => {
      const pkg = await readpkg();
      return `${pkg.name} ${pkg.version}`;
    });

    app.post('/webhook', async(req, reply) => {
      const pkg = await readpkg();
      lineHandler(req, reply);
      return `${pkg.name} ${pkg.version}`;
    });

    app.register(routerV1, { prefix: '/v1' });
    app.setErrorHandler(errorHandler);

    await app.listen(listeningPort, webhookRouterV1.name);
      // @ts-ignore
    app.blipp();
    app.swagger();

    const { port } = app.server.address() as AddressInfo;
    console.log(`server listening at :${port}`);
    let x = JSON.stringify(app.server.address());

    console.log('Address : ' + x);
  } catch (error) {
    console.log('server error', error.message);
    process.exit(1);
  }
  console.log('End initDatabase');
  // initDatabase(async () => {
  // });
};

startServer(3001);

// async function handleEvent(event) {
//   console.log('Begin handle Event');
//   switch (event.type) {
//     case 'message':
//       const message = event.message;
//       switch (message.type) {
//         case 'text':
//           return handleText(message, event);
//         case 'sticker':
//           return handleSticker(message, event);
//         default:
//           throw new Error(`Unknown message: ${JSON.stringify(message)}`);
//       }
//     default:
//       throw new Error(`Unknown event: ${JSON.stringify(event)}`);
//   }
// }

// async function handleText(message, event) {
//   console.log('Begin handle Text');
//   return null;
//   // return client.replyMessage(event.replyToken,  { type: 'text', text: message.text }); 
// }

// function handleSticker(message, event) {
//   console.log('Begin handle Sticker');
//   return null;
//   // return client.replyMessage(event.replyToken, { type: 'text', text: 'Got Image' });
// }
