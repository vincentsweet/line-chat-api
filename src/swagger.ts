import * as swagger from 'fastify-swagger';
import config from 'config';

export default swagger;

export const swaggerOptions = {
  swagger: {
    info: {
      title: 'EMETWORKS API',
      description: '',
      version: '0.0.1',
    },
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here',
    },
    host: config.swagger.host,
    schemes: config.swagger.schemes,
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [],
  },
  exposeRoute: true,
  routePrefix: '/docs',
};
