import { remove } from 'lodash';
import * as humps from 'humps';
import userModel from 'model/user';
import { isObjectEmpty, isArrayEmpty } from 'utility/common';
import { HttpRequestException } from 'exception/httpRequestException';
import { IUserRepository, UserModel, AddressModel } from './user.d';

export class UserRepository implements IUserRepository {
  async getUserModelByLineUserId(lineUserId: string) {
    const userData = await userModel
      .getModel()
      .findOne({ line_user_id: lineUserId })
      .exec();

    if (isObjectEmpty(userData)) {
      throw new HttpRequestException('901_USER_NOT_EXIST');
    }

    return userData;
  }

  async clearDefault(userModelData) {
    userModelData.addresses.forEach(a => (a.is_default = false));
  }

  async addAddress(lineUserId: string, data: AddressModel): Promise<void> {
    const userData = await this.getUserModelByLineUserId(lineUserId);

    if (data.isDefault && !isArrayEmpty(userData.addresses)) {
      this.clearDefault(userData);
    }

    if (isArrayEmpty(userData.addresses)) {
      data.isDefault = true;
      userData.addAddress = [humps.decamelizeKeys(data)];
    } else {
      userData.addresses.push(humps.decamelizeKeys(data));
    }

    await userData.save();
  }

  async updateAddress(lineUserId: string, data: AddressModel): Promise<void> {
    const userData = await this.getUserModelByLineUserId(lineUserId);
    const address = userData.addresses.find(
      a => a.address_id.toString() === data.addressId,
    );

    if (!isArrayEmpty(userData.addresses)) {
      if (data.isDefault) {
        this.clearDefault(userData);
      } else if (address.is_default) {
        throw new HttpRequestException('102_USER_MUST_HAVE_A_DEFAULT_ADDRESS');
      }

      address.name = data.name;
      address.is_default = data.isDefault;
      address.name = data.name;
      address.mobile_no = data.mobileNo;
      address.address = data.address;
      address.district = data.district;
      address.sub_district = data.subDistrict;
      address.province = data.province;
      address.zip_code = data.zipCode;
    } else {
      data.isDefault = true;
      userData.addAddress = [humps.decamelizeKeys(data)];
    }

    await userData.save();
  }

  async deleteAddress(lineUserId: string, addressId: string): Promise<void> {
    const userData = await this.getUserModelByLineUserId(lineUserId);
    const address = userData.addresses.find(
      a => a.address_id.toString() === addressId,
    );

    if (isObjectEmpty(address)) {
      throw new HttpRequestException('101_CANNOT_FIND_USER_ADDRESS');
    } else if (address.is_default) {
      throw new HttpRequestException('100_CANNOT_DELETE_A_DEFAULT_ADDRESS');
    } else {
      userData.addresses = remove(
        userData.addresses,
        // @ts-ignore
        a => a.address_id.toString() !== addressId,
      );
      await userData.save();
    }
  }

  async getUserByLineUserId(lineUserId: string): Promise<UserModel> {
    const user = await this.getUserModelByLineUserId(lineUserId);
    const { addresses, carts, ...userInfo } = user.toObject();
    const data = humps.camelizeKeys(userInfo);
    data.addresses = humps.camelizeKeys(addresses || []);
    data.carts = humps.camelizeKeys(carts || []);
    return data;
  }

  async createUser(data: UserModel): Promise<void> {
    const user = humps.decamelizeKeys(data);
    userModel.getModel().create(user);
  }
}

export const userRepository = new UserRepository();
export default userRepository;
