export interface AddressModel {
  addressId: string;
  isDefault: boolean;
  name: string;
  mobileNo: string;
  address: string;
  district: string;
  subDistrict: string;
  province: string;
  zipCode: string;
}

export interface TaxModel {
  id: string;
  name: string;
  address: string;
  district: string;
  subDistrict: string;
  province: string;
  zipCode: string;
}

export interface CartModel {
  productId: string;
  qty: number;
  createDate?: Date;
}

export interface UserModel {
  lineUserId: string;
  firstName: string;
  lastName: string;
  mobileNo: string;
  email: string;
  image: string;
  isVerify: boolean;
  tax: TaxModel;
  addresses: AddressModel[];
}

export interface IUserRepository {
  getUserByLineUserId(lineUserId: string): Promise<UserModel>;
  createUser(data: UserModel): Promise<void>;
  addAddress(lineUserId: string, data: AddressModel): Promise<void>;
  updateAddress(lineUserId: string, data: AddressModel): Promise<void>;
  deleteAddress(lineUserId: string, addressId: string): Promise<void>;
}
