import { isNil, isEmpty } from 'lodash';
import config from 'config';

export function consoleLog(data: any, alwayShow?: boolean) {
  if (config.env !== 'production' || alwayShow) {
    console.log(data);
  }
}

export function isObjectEmpty(obj: any) {
  return isNil(obj) || isEmpty(obj);
}

export function isArrayEmpty(obj: any) {
  return !(obj && Array.isArray(obj) && obj.length);
}
