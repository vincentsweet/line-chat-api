import { RouteShorthandOptions } from 'fastify';
import headerSchema from 'router/schema/common/header';
import error400Schema from 'router/schema/error/400';
import error500Schema from 'router/schema/error/500';
import { isArrayEmpty, isObjectEmpty } from './common';

export function buildSchema(
  options: RouteShorthandOptions,
  tagName: string | string[],
): RouteShorthandOptions {
  let { schema } = options;

  if (isObjectEmpty(schema.tags) || isArrayEmpty(schema.tags)) {
    schema.tags = (isArrayEmpty(tagName) ? [tagName] : tagName) as string[];
  }

  if (isObjectEmpty(schema.headers)) {
    schema.headers = headerSchema;
  }

  if (schema.response && isObjectEmpty(schema.response['400'])) {
    schema.response = {
      ...schema.response,
      ...error400Schema,
    };
  }

  if (schema.response && isObjectEmpty(schema.response['500'])) {
    schema.response = {
      ...schema.response,
      ...error500Schema,
    };
  }

  return options;
}
