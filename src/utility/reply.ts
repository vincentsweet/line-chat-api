import { FastifyReply } from 'fastify';
import * as humps from 'humps';

export const ReplyData = (reply: FastifyReply<any>, data: any) => {
  reply.send({ data: humps.decamelizeKeys(JSON.parse(JSON.stringify(data))) });
};
