import { Client, Message } from '@line/bot-sdk';
import config from '../config';
import { userInfo } from 'os';
import user from 'router/v1/user';
import { getUserByLineUserId } from 'router/v1/user/getUserBylineUserId';
import { IncomingMessage } from 'http';
import * as l from 'router/v1/webhook/line';
// import httpRequest from './http';

interface LineUserProfile {
  displayName: string;
  userId: string;
  pictureUrl: string;
  statusMessage: string;
}

export class Line {
  lineClient = new Client(config.line);

  pushTextMessage(userId: string, message: string) {
    return this.lineClient.pushMessage(userId, {
      type: 'text',
      text: message,
    });
  }

  pushMessage(userId: string, messages: any) {
    return this.lineClient.pushMessage(userId, messages);
  }

  replyTextMessage(token: string, messages: string) {
    return this.lineClient.replyMessage(token, {
      type: 'text',
      text: messages,
    });
  }

  replyMessage(token: string, messages: any) {
    return this.lineClient.replyMessage(token, messages);
  }

  getMessageContent(messageId: string) {
    return this.lineClient.getMessageContent(messageId);
  }

  getProfile(userId: string) {
    return this.lineClient.getProfile(userId);
  }

  async getUserProfile(
    type: string,
    lineGroupId: string,
    lineUserId: string,
  ): Promise<LineUserProfile> {
    let profile: LineUserProfile = undefined;
    try {
      if (type === 'GROUP') {
        profile = await this.lineClient.getGroupMemberProfile(
          lineGroupId,
          lineUserId,
        );
      } else {
        profile = await this.lineClient.getRoomMemberProfile(
          lineGroupId,
          lineUserId,
        );
      }
      console.log('profile', profile);
    } catch (err) {
      console.log(`Cannot get a user's profile: ${err.message}`);
    }
    return profile;
  }

}

export const lineUtil = new Line();
export default lineUtil;
