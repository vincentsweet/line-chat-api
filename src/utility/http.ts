import * as request from 'request-promise';
import { isObjectEmpty } from './common';
export enum HttpMethod {
  get = 'GET',
  post = 'POST',
  put = 'PUT',
  delete = 'DELETE',
}

export class HttpRequest {
  async _request(
    uri: string,
    body: any = null,
    options: {
      token?: string;
      method: HttpMethod;
    },
  ) {
    const headers = {
      'content-type': 'application/json',
      Authorization: options.token ? `Bearer ${options.token}` : '',
    };

    const reqOption = {
      method: options.method,
      uri,
      headers,
      body,
    };

    const data = await request(reqOption);
    if (isObjectEmpty(data)) {
      return {};
    }
    return JSON.parse(data);
  }

  get(url: string, token?: string) {
    return this._request(url, undefined, {
      method: HttpMethod.get,
      token,
    });
  }

  post(url: string, body?: any, token?: string) {
    return this._request(url, body, {
      method: HttpMethod.post,
      token,
    });
  }

  delete(url: string, body?: any, token?: string) {
    return this._request(url, body, {
      method: HttpMethod.delete,
      token,
    });
  }
}

export const httpRequest = new HttpRequest();
export default httpRequest;
