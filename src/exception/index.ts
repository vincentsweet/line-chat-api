import { isObjectEmpty } from 'utility/common';
import { HttpRequestException } from './httpRequestException';
import { HttpInternalException } from './httpInternalException';
import { PermissionException } from './permissionException';

export function errorHandler(error, req, reply) {
  console.log('error =----> ', error);
  try {
    if (!isObjectEmpty(reply)) {
      if (error instanceof HttpRequestException) {
        reply.status(400).send({
          message: error.message,
          status_code: error.code,
        });
      } else if (error instanceof PermissionException) {
        reply.status(403).send({
          message: 'PERMISSION_DENIED',
          status_code: '403',
        });
      } else if (error instanceof HttpInternalException) {
        throw error;
      } else {
        throw new HttpInternalException();
      }
    } else {
      throw new HttpInternalException();
    }
  } catch (error) {
    reply.status(500).send({
      message: 'Internal Server Error!',
      status_code: 500,
    });
  }
}
