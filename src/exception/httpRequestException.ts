import { BaseException } from './baseException';

export class HttpRequestException extends BaseException {
  code: String;
  message: String;

  constructor(code: string, message?: string, error?: Error) {
    super();

    console.log(error);
    this.code = code;
    this.message = message;
  }
}
