import { BaseException } from './baseException';

export class PermissionException extends BaseException {
  constructor(error?: Error) {
    super();

    console.log(error);
  }
}
