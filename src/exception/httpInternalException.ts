import { BaseException } from './baseException';

export class HttpInternalException extends BaseException {
  constructor(error?: Error) {
    super();

    console.log(error);
  }
}
