export class BaseException {
  constructor() {
    Error.apply(this, arguments);
  }
}

BaseException.prototype = new Error();
