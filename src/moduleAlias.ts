require('module-alias/register');

import * as moduleAlias from 'module-alias';
import config from './config';
import * as admin from 'firebase-admin';
const serviceAccount = require('../chat-tool-api-develop/chatbot-line-api-2844d5137231.json');

export const initModuleAlias = () => {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://chatbot-line-api.firebaseio.com'
  });
  if (config.env !== 'localhost') {
    moduleAlias.addAlias('config', `${__dirname}/config`);
    moduleAlias.addAlias('exception', `${__dirname}/exception`);
    moduleAlias.addAlias('router', `${__dirname}/router`);
    moduleAlias.addAlias('model', `${__dirname}/model`);
    moduleAlias.addAlias('repository', `${__dirname}/repository`);
    moduleAlias.addAlias('utility', `${__dirname}/utility`);
    moduleAlias();
  }

};
