import * as mongoose from 'mongoose';

export interface IAddress extends mongoose.Document {
  address_id: string;
  is_default: boolean;
  name: string;
  mobile_no: string;
  address: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
}

export interface ITax extends mongoose.Document {
  id: string;
  name: string;
  address: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
}

export interface IUser extends mongoose.Document {
  line_user_id: string;
  first_name: string;
  last_name: string;
  mobile_no: string;
  email: string;
  image: string;
  is_verify: boolean;
  tax: ITax;
  addresses: IAddress[];
}
