import * as mongoose from 'mongoose';

export interface IModel {
  getSchema(): mongoose.Schema;
  getModel(): mongoose.Model<mongoose.Document>;
}
