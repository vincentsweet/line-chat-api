import * as mongoose from 'mongoose';
import * as timestamp from 'mongoose-timestamp';
import { collection } from './collection';
import { isObjectEmpty } from 'utility/common';
import { IModel } from './model';
import { IUser } from './user.d';

class UserModel implements IModel {
  schema: mongoose.Schema;

  getSchema() {
    if (isObjectEmpty(this.schema)) {
      const { ObjectId } = mongoose.Schema.Types;

      const taxSchema = new mongoose.Schema(
        {
          id: {
            type: String,
            required: [true, 'ID_IS_A_MANDATORY_FIELD'],
          },
          name: {
            type: String,
            required: [true, 'NAME_IS_A_MANDATORY_FIELD'],
          },
          address: {
            type: String,
            required: [true, 'ADDRESS_IS_A_MANDATORY_FIELD'],
          },
          district: {
            type: ObjectId,
            required: [true, 'DISTRICT_IS_A_MANDATORY_FIELD'],
          },
          sub_district: {
            type: ObjectId,
            required: [true, 'SUB_DISTRICT_IS_A_MANDATORY_FIELD'],
          },
          province: {
            type: ObjectId,
            required: [true, 'PROVINCE_IS_A_MANDATORY_FIELD'],
          },
          zip_code: {
            type: String,
            required: [true, 'ZIP_CODE_IS_A_MANDATORY_FIELD'],
          },
        },
        { _id: false },
      );
      taxSchema.plugin(timestamp, {
        createdAt: 'created_date',
      });

      const addressSchema = new mongoose.Schema(
        {
          address_id: {
            type: ObjectId,
            required: [true, 'ADDRESS_ID_IS_A_MANDATORY_FIELD'],
            auto: true,
          },
          is_default: {
            type: Boolean,
            default: false,
          },
          name: {
            type: String,
            required: [true, 'NAME_IS_A_MANDATORY_FIELD'],
          },
          mobile_no: {
            type: String,
            required: [true, 'MOBILE_NO_IS_A_MANDATORY_FIELD'],
          },
          address: {
            type: String,
            required: [true, 'ADDRESS_IS_A_MANDATORY_FIELD'],
          },
          district: {
            type: ObjectId,
            required: [true, 'DISTRICT_IS_A_MANDATORY_FIELD'],
          },
          sub_district: {
            type: ObjectId,
            required: [true, 'SUB_DISTRICT_IS_A_MANDATORY_FIELD'],
          },
          province: {
            type: ObjectId,
            required: [true, 'PROVINCE_IS_A_MANDATORY_FIELD'],
          },
          zip_code: {
            type: String,
            required: [true, 'ZIP_CODE_IS_A_MANDATORY_FIELD'],
          },
        },
        { _id: false },
      );
      addressSchema.plugin(timestamp, {
        createdAt: 'created_date',
        updatedAt: 'updated_date',
      });

      this.schema = new mongoose.Schema({
        line_user_id: {
          type: String,
          required: [true, 'LINE_ID_IS_A_MANDATORY_FIELD'],
          unique: true,
        },
        first_name: {
          type: String,
          required: [true, 'FIRST_NAME_IS_A_MANDATORY_FIELD'],
        },
        last_name: {
          type: String,
        },
        mobile_no: {
          type: String,
        },
        email: {
          type: String,
        },
        image: {
          type: String,
        },
        is_verify: {
          type: Boolean,
          default: false,
        },
        tax: {
          type: taxSchema,
        },
        addresses: {
          type: [addressSchema],
        },
      });
      this.schema.plugin(timestamp, {
        createdAt: 'created_date',
        updatedAt: 'updated_date',
      });
    }

    return this.schema;
  }

  getModel(): mongoose.Model<IUser> {
    const model = mongoose.model<IUser>(collection.user, this.getSchema());
    return model;
  }
}

export const model = new UserModel();
export default model;
