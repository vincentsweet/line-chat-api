export default {
  limit: {
    type: 'number',
  },
  offset: {
    type: 'number',
  },
};
