export default {
  type: 'object',
  properties: {
    authorization: {
      type: 'string',
    },
  },
};
