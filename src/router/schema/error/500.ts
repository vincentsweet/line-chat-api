export default {
  500: {
    type: 'object',
    properties: {
      status_code: { type: 'string' },
      message: { type: 'string' },
    },
  },
};
