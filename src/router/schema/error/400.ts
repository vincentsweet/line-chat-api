export default {
  400: {
    type: 'object',
    properties: {
      status_code: { type: 'string' },
      message: { type: 'string' },
    },
  },
};
