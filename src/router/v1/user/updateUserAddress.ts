import { RouteShorthandOptions } from 'fastify';
import { ReplyData } from 'utility/reply';
import { isObjectEmpty } from 'utility/common';
import { HttpRequestException } from 'exception/httpRequestException';
import { PermissionException } from 'exception/permissionException';
import userRepository from 'repository/user';
import address from './schema/address';

export const options: RouteShorthandOptions = {
  schema: {
    body: address,
    response: {
      200: {
        type: 'object',
        properties: {
          data: {
            type: 'object',
            properties: {
              status: { type: 'string', enum: ['S'] },
            },
          },
        },
      },
    },
  },
};

export async function updateUserAddress(req, reply) {
  const { user, body } = req;
  if (isObjectEmpty(user) && !user.lineUserId) {
    throw new PermissionException();
  }

  if (isObjectEmpty(body)) {
    throw new HttpRequestException('101_NO_REQUEST_BODY');
  }

  await userRepository.updateAddress(user.lineUserId, body);

  ReplyData(reply, {
    status: 'S',
  });
}

export default {
  options,
  handler: updateUserAddress,
};
