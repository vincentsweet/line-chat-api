import { FastifyInstance } from 'fastify';
import { buildSchema } from 'utility/schema';
import getUserBylineUserId from './getUserBylineUserId';
import createUserAddress from './createUserAddress';
import updateUserAddress from './updateUserAddress';
import deleteUserAddress from './deleteUserAddress';

export default (fastify: FastifyInstance, opts, next) => {
  const tagName = 'users v.1';
  fastify.get(
    '/:lineUserId',
    buildSchema(getUserBylineUserId.options, tagName),
    getUserBylineUserId.handler,
  );
  fastify.post(
    '/addresses',
    buildSchema(createUserAddress.options, tagName),
    createUserAddress.handler,
  );
  fastify.put(
    '/addresses',
    buildSchema(updateUserAddress.options, tagName),
    updateUserAddress.handler,
  );
  fastify.delete(
    '/addresses',
    buildSchema(deleteUserAddress.options, tagName),
    deleteUserAddress.handler,
  );

  next();
};
