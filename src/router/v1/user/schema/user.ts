import taxSchema from './tax';
import addressSchema from './address';

export default {
  type: 'object',
  properties: {
    line_user_id: { type: 'string' },
    first_name: { type: 'string' },
    last_name: { type: 'string' },
    mobile_no: { type: 'string' },
    email: { type: 'string' },
    image: { type: 'string' },
    is_verify: { type: 'boolean' },
    tax: taxSchema,
    addresses: { type: 'array', items: addressSchema },
    created_date: { type: 'string' },
    updated_date: { type: 'string' },
  },
};
