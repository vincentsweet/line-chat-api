export default {
  type: 'object',
  properties: {
    address_id: { type: 'string' },
    is_default: { type: 'boolean' },
    name: { type: 'string' },
    mobile_no: { type: 'string' },
    address: { type: 'string' },
    district: { type: 'string' },
    sub_district: { type: 'string' },
    province: { type: 'string' },
    zip_code: { type: 'string' },
  },
};
