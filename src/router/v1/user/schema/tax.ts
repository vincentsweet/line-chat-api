export default {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    address: { type: 'string' },
    district: { type: 'string' },
    sub_district: { type: 'string' },
    province: { type: 'string' },
    zip_code: { type: 'string' },
  },
};
