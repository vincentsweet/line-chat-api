import { RouteShorthandOptions } from 'fastify';
import { ReplyData } from 'utility/reply';
import { isObjectEmpty } from 'utility/common';
import { HttpRequestException } from 'exception/httpRequestException';
import { PermissionException } from 'exception/permissionException';
import userRepository from 'repository/user';

export const options: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      properties: {
        address_id: { type: 'string' },
      },
    },
    response: {
      200: {
        type: 'object',
        properties: {
          data: {
            type: 'object',
            properties: {
              status: { type: 'string', enum: ['S'] },
            },
          },
        },
      },
    },
  },
};

export async function deleteUserAddress(req, reply) {
  const { user, body } = req;
  if (isObjectEmpty(user) && !user.lineUserId) {
    throw new PermissionException();
  }

  if (isObjectEmpty(body)) {
    throw new HttpRequestException('101_NO_REQUEST_BODY');
  }

  await userRepository.deleteAddress(user.lineUserId, body.addressId);

  ReplyData(reply, {
    status: 'S',
  });
}

export default {
  options,
  handler: deleteUserAddress,
};
