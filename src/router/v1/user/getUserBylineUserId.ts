import { RouteShorthandOptions } from 'fastify';
import { ReplyData } from 'utility/reply';
import { isObjectEmpty } from 'utility/common';
import { HttpRequestException } from 'exception/httpRequestException';
import userRepository from 'repository/user';
import userSchema from './schema/user';

export const options: RouteShorthandOptions = {
  schema: {
    params: {
      type: 'object',
      properties: {
        lineUserId: { type: 'string' },
      },
    },
    response: {
      200: {
        type: 'object',
        properties: {
          data: userSchema,
        },
      },
    },
  },
};

export async function getUserByLineUserId(req, reply) {
  const { lineUserId } = req.params;

  if (!lineUserId) {
    throw new HttpRequestException(
      '101_MISSING_PARAM',
      'Line UserID is a mandatory field.',
    );
  }

  const user = await userRepository.getUserByLineUserId(lineUserId);

  if (isObjectEmpty(user)) {
    throw new HttpRequestException(
      '901_USER_NOT_EXIST',
      'User does not exist.',
    );
  }

  ReplyData(reply, user);
}

export default {
  options,
  handler: getUserByLineUserId,
};
