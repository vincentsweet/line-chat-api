import { FastifyInstance } from 'fastify';
import lineHandler from './line';
import { buildSchema } from 'utility/schema';

export default (fastify: FastifyInstance, opts, next) => {
  const tagName = 'webhook v.1';
  fastify.post(
    '/line',
    buildSchema(lineHandler.options, tagName),
    lineHandler.handler,
  );
  next();
};
