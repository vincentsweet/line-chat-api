import line from 'utility/line';
import * as admin from 'firebase-admin';

export default event => {
  const { text } = event.message;
  const { userId } = event.source;
  console.log('event =----> ', event);
  console.log('replyToken =----> ', event.replyToken);
  console.log('text =----> ', text);
  console.log('userId =----> ', userId);

  const db = admin.firestore();
  console.log('db.. ' + JSON.stringify(db));
  const chatlog = {
    userId : userId,
    text: text,
    date : new Date()
  };

  db.collection('chatlog').add(chatlog).then(resp => {
    console.log('log complete');
    switch (text.toLowerCase()) {
      case 'userid':
        line.replyTextMessage(event.replyToken, userId);
        break;
      default:
        line.replyTextMessage(event.replyToken, text);
        break;
    }
  }).catch(err => {
    console.error('error firebase' , err);
  });
};

function truncate(n, useWordBoundary) {
  if (this.length <= n) {
    return this;
  }
  const subString = this.substr(0, n - 1);
  return (
    (useWordBoundary
      ? subString.substr(0, subString.lastIndexOf(' '))
      : subString) + '...'
  );
}
