import line from 'utility/line';
import { isObjectEmpty } from 'utility/common';

export default event => {
  const { userId } = event.source;
  if (!isObjectEmpty(userId)) {
    // create user
  }
};
