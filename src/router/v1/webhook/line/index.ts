import { RouteShorthandOptions } from 'fastify';
import { ReplyData } from 'utility/reply';
import { isArrayEmpty } from 'utility/common';
import { HttpRequestException } from 'exception/httpRequestException';
import { HttpInternalException } from 'exception/httpInternalException';
import handlerTextMessage from './handlerTextMessage';
import handlerImageMessage from './handlerImageMessage';
import handlerFollowEvent from './handlerFollowEvent';

export const options: RouteShorthandOptions = {
  schema: {},
};

export async function lineHandler(req, reply) {
  const { events } = req.body;
  console.log(events);

  if (isArrayEmpty(events)) {
    throw new HttpRequestException('101_INVALID_WEBHOOK');
  }

  try {
    await Promise.all(
      events
        // .filter(e => e.source.userId !== 'IGNORE-LINE-ID')
        .map(event => {
          const { type, message } = event;
          console.log(event);
          switch (type) {
            case 'message':
              switch (message.type) {
                case 'text':
                  handlerTextMessage(event);
                  break;
                case 'image':
                  handlerImageMessage(event);
                  break;
                default:
                  return;
              }
              break;
            case 'unfollow':
              console.log('unfollow');
              break;
            case 'follow':
              handlerFollowEvent(event);
              break;
            default:
              return;
          }
        }),
    );
    ReplyData(reply, { status: 'success' });
  } catch (error) {
    throw new HttpInternalException(error);
  }
}

export default {
  options,
  handler: lineHandler,
};
